import 'package:flutter/material.dart';
import 'package:mytoolbox/Database/ToolBoxMenuDatabase/ToolBoxPositionDataModel.dart';
import 'package:mytoolbox/Database/ToolBoxMenuDatabase/toolBoxPositionOperation.dart';
import 'package:mytoolbox/MyTools/Calculator.dart';
import 'package:mytoolbox/MyTools/IToolPage.dart';
import 'package:mytoolbox/MyTools/MyLocation.dart';
import 'package:mytoolbox/MyTools/ToDo.dart';
import 'package:reorderables/reorderables.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Alet Çantam',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(
        title: 'MyToolBox',
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double toolBoxWidth;
  double toolBoxHeight;

  List<IToolPage> myToolList;
  List<int> myToolListOrder;
  List<Widget> widgetToolBox;

  _MyHomePageState() {
    toolBoxWidth = 80;
    toolBoxHeight = 100;
    widgetToolBox = <Widget>[];
    myToolList = [
      ToolCalculator(),
      ToolToDo(),
      ToolToDo(),
      ToolToDo(),
      ToolToDo(),
      ToolCalculator(),
      ToolCalculator(),
      ToolCalculator(),
      MyLocation(),
      MyLocation(),
      MyLocation(),
      MyLocation(),
      ToolCalculator(),
      ToolCalculator(),
    ];
    myToolListOrder = [];
    _setPositionFromDatabase();
  }

  _setPositionFromDatabase() {
    ToolBoxPositionOperation.db.getPositionList().then((positionList) {
      myToolListOrder.clear();
      if (positionList.length > myToolList.length) {
        ToolBoxPositionOperation.db.clearTable().then((value) {
          _setPositionFromDatabase();
        });
        return;
      } else if (positionList.length < myToolList.length) {
        // yeni sayfa eklendi
        for (int i = 0; i < positionList.length; i++) {
          ToolBoxPositionOperation.db.addOrUpdatePosition(
              ToolboxPositionDataModel(i, positionList[i].position));
          myToolListOrder.add(positionList[i].position);
        }

        for (int i = positionList.length; i < myToolList.length; i++) {
          ToolBoxPositionOperation.db.addOrUpdatePosition(
              ToolboxPositionDataModel(i + positionList.length, i));
          myToolListOrder.add(i);
        }
      } else
        myToolListOrder = positionList.map((e) => e.position).toList();

      if (myToolListOrder.toSet().length != myToolListOrder.length) {
        //pozisyonlarda tekrar eden eleman var ise database temizleniyor.
        ToolBoxPositionOperation.db.clearTable().then((value) {
          _setPositionFromDatabase();
        });
        return;
      }
      widgetToolBox.clear();
      for (int i = 0; i < myToolList.length; i++) {
        if (myToolListOrder[i] >= myToolList.length) {
          // eğer elimizdeki sayfa sayısından daha büyük bir pozisyon var ise database i sıfırlıyor.
          ToolBoxPositionOperation.db.clearTable().then((value) {
            _setPositionFromDatabase();
          });
          return;
        }
        widgetToolBox.add(toolBoxNode(myToolList[myToolListOrder[i]]));
      }
      setState(() {});
    });
  }

  _onReorder(int oldIndex, int newIndex) {
    setState(() {
      Widget tempWidget = widgetToolBox.removeAt(oldIndex);
      widgetToolBox.insert(newIndex, tempWidget);

      int tempInt = myToolListOrder.removeAt(oldIndex);
      myToolListOrder.insert(newIndex, tempInt);

      for (int i = 0; i < myToolListOrder.length; i++) {
        ToolBoxPositionOperation.db.addOrUpdatePosition(
            ToolboxPositionDataModel(i, myToolListOrder[i]));
      }
    });
  }

  gridViewSelectedItem(BuildContext context, IToolPage toolItem) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => toolItem));
  }

  Widget toolBoxNode(IToolPage item) {
    return Container(
      color: Colors.transparent,
      width: toolBoxWidth,
      child: GestureDetector(
        onTap: () {
          gridViewSelectedItem(context, item);
        },
        child: Center(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: item.color,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: AspectRatio(
                  aspectRatio: 1,
                  child: Image(
                    image: item.iconProvider,
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ),
              Container(
                color: Colors.transparent,
                child: Text(item.name,
                    style: TextStyle(fontSize: 13, color: Colors.black),
                    textAlign: TextAlign.center),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Color(0xffF0F0F0), Color(0xffF2F2F2)])),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ReorderableWrap(
                spacing: 10.0,
                runSpacing: 10.0,
                padding: EdgeInsets.all(10),
                children: widgetToolBox,
                onReorder: _onReorder,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
