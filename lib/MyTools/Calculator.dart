import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:mytoolbox/MyTools/IToolPage.dart';
import 'package:mytoolbox/MyTools/CustomWidget/CustomButton.dart';

class ToolCalculator extends IToolPage {
  ToolCalculator()
      : super(
            name: "Hesap Makinesi",
            iconProvider: AssetImage("assets/icons/icon_calculator.png"),
            color: Colors.transparent);
  _ToolCalculatorState createState() => _ToolCalculatorState();
}

class ButtonItem {
  String text;
  int value;
  int backColor;
  int backColorAnim;
  int textColor;
  ButtonItem(this.value, this.text,
      {this.backColor = 0xfff3f2f3,
      this.backColorAnim = 0xffcfcfcf,
      this.textColor = 0xff000000});
}

class _ToolCalculatorState extends IToolPageState {
  final _myTextFieldController = TextEditingController();
  bool isResultVisible = false;
  List<Widget> buttonList;
  _ToolCalculatorState() {
    FocusManager.instance.primaryFocus.unfocus();
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    buttonList = [
      customButtonNode(ButtonItem(-1, "⟵", textColor: 0xff68b11d)),
      customButtonNode(ButtonItem(-2, "C" , textColor: 0xffe3816e)),
      customButtonNode(ButtonItem(-3, "%", textColor: 0xff68b11d)),
      customButtonNode(ButtonItem(-4, "/", textColor: 0xff68b11d)),
      customButtonNode(ButtonItem(7, "7")),
      customButtonNode(ButtonItem(8, "8")),
      customButtonNode(ButtonItem(9, "9")),
      customButtonNode(ButtonItem(-5, "*", textColor: 0xff68b11d)),
      customButtonNode(ButtonItem(4, "4")),
      customButtonNode(ButtonItem(5, "5")),
      customButtonNode(ButtonItem(6, "6")),
      customButtonNode(ButtonItem(-6, "-", textColor: 0xff68b11d)),
      customButtonNode(ButtonItem(1, "1")),
      customButtonNode(ButtonItem(2, "2")),
      customButtonNode(ButtonItem(3, "3")),
      customButtonNode(ButtonItem(-7, "+", textColor: 0xff68b11d)),
      customButtonNode(ButtonItem(10, "( )")),
      customButtonNode(ButtonItem(0, "0")),
      customButtonNode(ButtonItem(-8, ".")),
      customButtonNode(ButtonItem(-9, "=",
          backColor: 0xff67b21b,
          backColorAnim: 0xff538e15,
          textColor: Colors.white.value)),
    ];
  }

  Widget customButtonNode(ButtonItem item) {
    return CustomButton(
      text: item.text,
      backColor: Color(item.backColor),
      backColorAnim: Color(item.backColorAnim),
      textColor: Color(item.textColor),
      width: 80,
      heigth: 60,
      margin: 5,
      padding: 18,
      onTap: () {
        _buttonClickEvent(item);
      },
      onLongPressRepeat: () {
        _onLongPressRepeat(item);
      },
    );
  }

  _onLongPressRepeat(ButtonItem item) {
    if (item.value == -1) {
      _buttonClickEvent(item);
    }
  }

  _buttonClickEvent(ButtonItem item) {
    if (_myTextFieldController.text.isEmpty && item.value < 0) {
      return;
    } else if (isResultVisible) {
      _myTextFieldController.text = _myTextFieldController.text.split("\n=")[0];
      isResultVisible = false;
    }
    switch (item.value) {
      case -1: // <-
        if (_myTextFieldController.selection.baseOffset !=
            -1) // cursor secildiyse, textfield focus olmadıgında -1 oluyor
        {
          int tempOffset = _myTextFieldController.selection.baseOffset;
          _myTextFieldController.text = _myTextFieldController.text.substring(
                  0, _myTextFieldController.selection.baseOffset - 1) +
              _myTextFieldController.text.substring(
                  _myTextFieldController.selection.baseOffset,
                  _myTextFieldController.text.length);
          _myTextFieldController.selection =
              TextSelection.fromPosition(TextPosition(offset: tempOffset - 1));
        } else
          _myTextFieldController.text = _myTextFieldController.text
              .substring(0, _myTextFieldController.text.length - 1);
        break;
      case -2: // C
        _myTextFieldController.text = "";
        break;
      case -9: // =
        if (_myTextFieldController.text != "") {
          Parser p = Parser();
          Expression exp = p.parse(_myTextFieldController.text);
          ContextModel cm = ContextModel();
          _myTextFieldController.text +=
              "\n=" + exp.evaluate(EvaluationType.REAL, cm).toString();
          isResultVisible = true;
        }
        break;
      default:
        if (_myTextFieldController.selection.baseOffset !=
            -1) // cursor secildiyse
        {
          int tempOffset = _myTextFieldController.selection.baseOffset;
          _myTextFieldController.text = _myTextFieldController.text
                  .substring(0, _myTextFieldController.selection.baseOffset) +
              item.text +
              _myTextFieldController.text.substring(
                  _myTextFieldController.selection.baseOffset,
                  _myTextFieldController.text.length);
          _myTextFieldController.selection =
              TextSelection.fromPosition(TextPosition(offset: tempOffset + 1));
        } else
          _myTextFieldController.text += item.text;
        break;
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 20, right: 20),
                child: TextField(
                  showCursor: true,
                  readOnly: true,
                  controller: _myTextFieldController,
                  maxLines: null,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: '0',
                    isDense: true,
                  ),
                  style: TextStyle(
                    fontSize: 35.0,
                    color: Colors.black,
                  ),
                  textAlign: TextAlign.end,
                ),
              ),
            ),
            Center(
              child: ConstrainedBox(
                constraints: new BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height / 2,
                  maxWidth: MediaQuery.of(context).size.height / 2,
                ),
                child: GridView.count(
                  crossAxisCount: 4,
                  childAspectRatio: 1.3,
                  children: buttonList,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
