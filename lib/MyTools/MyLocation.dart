import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mytoolbox/Database/LocationModel/LocationItemModel.dart';
import 'package:mytoolbox/Database/LocationModel/LocationListPositionDataModel.dart';
import 'package:mytoolbox/Database/LocationModel/LocationOperation.dart';
import 'package:mytoolbox/MyTools/CustomWidget/CustomAddLocation.dart';
import 'package:mytoolbox/MyTools/CustomWidget/CustomLocationItem.dart';
import 'package:mytoolbox/MyTools/IToolPage.dart';

class MyLocation extends IToolPage {
  MyLocation()
      : super(
            name: "Konumlarim",
            iconProvider: AssetImage("assets/icons/icon_location.png"),
            color: Colors.transparent);

  _MyLocationState createState() => _MyLocationState();
}

class _MyLocationState extends IToolPageState {
  List<CustomLocationItem> _locationWidgets = [];

  _MyLocationState() {
    LocationOperation.db.getLocationList().then((dataModelList) {
      LocationOperation.db.getPositions().then((positions) {
        if (positions.length == dataModelList.length) {
          positions.forEach(
            (position) {
              _locationWidgets.add(
                _createListWidgetItem(
                  dataModelList.firstWhere(
                      (element) => element.locationId == position.locationId),
                ),
              );
            },
          );
        } else {
          _savePositions();
          _locationWidgets =
              dataModelList.map((e) => _createListWidgetItem(e)).toList();
        }
        setState(() {});
      });
    });
  }

  Future<bool> _checkPermission() async {
    LocationPermission permission;

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      locationPermissionDenied("Konum Servisine Ulaşılamıyor",
          "Uygulamanın bu özelliği için konum erişim izin vermelisiniz.");
      return false;
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        locationPermissionDenied("Konum Servisine Ulaşılamıyor",
            "Uygulamayı kullanabilmek için izin vermelisiniz.");
        return false;
      }
    }
    return true;
  }

  _savePositions() {
    LocationOperation.db.clearPositions().then((value) {
      _locationWidgets.forEach((element) {
        LocationOperation.db.addPosition(
            LocationListPositionDataModel(element.modelItem.locationId));
      });
    });
  }

  CustomLocationItem _createListWidgetItem(LocationItemDataModel modelItem) {
    return CustomLocationItem(
      key: ObjectKey(modelItem),
      modelItem: modelItem,
      deleteItem: _locationDelete,
    );
  }

  locationPermissionDenied(String title, String description) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(description),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Tamam'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _locationAdd(LocationItemDataModel dataModel) {
    LocationOperation.db.addLocation(dataModel).then((value) {
      _locationWidgets.add(_createListWidgetItem(value));
      setState(() {});
    });
  }

  _locationDelete(CustomLocationItem customLocationItem) {
    LocationOperation.db
        .removeLocation(customLocationItem.modelItem.locationId);
    _locationWidgets.remove(customLocationItem);
    setState(() {});
  }

  addNewLocation() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: CustomAddLocation(
            locationAdd: _locationAdd,
          ),
        );
      },
    );
  }

  _onReorder(int oldIndex, int newIndex) {
    if (newIndex > oldIndex) {
      // bu kısım olmaz ise bazı durumlarda iki item üst üste biniyor
      newIndex -= 1;
    }

    Widget tempWidget = _locationWidgets.removeAt(oldIndex);
    _locationWidgets.insert(newIndex, tempWidget);
    _savePositions();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _checkPermission().then((value) {
            if (value) {
              addNewLocation();
            }
          });
        },
        child: Text(
          "+",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        child: Theme(
          data: ThemeData(canvasColor: Colors.transparent),
          child: ReorderableListView(
            padding: EdgeInsets.only(top: 5),
            onReorder: _onReorder,
            children: _locationWidgets,
          ),
        ),
      ),
    );
  }
}
