import 'package:flutter/material.dart';

abstract class IToolPage extends StatefulWidget{
  @mustCallSuper
  IToolPage({Key key, this.name, this.iconProvider, this.colorMenuButton, this.color}) : super(key: key);
  final String name;
  final ImageProvider iconProvider;
  final Color color;
  final Color colorMenuButton;
}

abstract class IToolPageState extends State<IToolPage> {
  
}