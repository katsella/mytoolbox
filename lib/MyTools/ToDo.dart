import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:mytoolbox/Database/todoDataModel/TodoIPanelPositionDataModel.dart';
import 'package:mytoolbox/Database/todoDataModel/TodoOperation.dart';
import 'package:mytoolbox/Database/todoDataModel/TodoPanelItemDataModel.dart';
import 'package:mytoolbox/MyTools/CustomWidget/CustomPanel.dart';
import 'package:mytoolbox/MyTools/CustomWidget/CustomPanelItem.dart';
import 'package:mytoolbox/MyTools/IToolPage.dart';

class ToolToDo extends IToolPage {
  ToolToDo()
      : super(
            name: "Yapılacaklar",
            iconProvider: AssetImage("assets/icons/icon_todo.png"),
            color: Colors.transparent);
  _ToolToDoState createState() => _ToolToDoState();
}

class _ToolToDoState extends IToolPageState {
  List<CustomPanel> todoPanelWidgets;
  bool _isTodoItemSelected = false;
  CustomPanelItem _selectedCustomPanelItem;
  CustomPanelController _selectedCustomPanelController;
  double _animOpacity = 0.0;
  double _floatMarginBottom = 0.0;
  int _animOpacityMilis = 200;
  TextEditingController _textEditingController = TextEditingController();
  Color pickerColor = Color(0xff443a49);
  DecorationImage _backDecoration;
  _ToolToDoState() {
    todoPanelWidgets = [];

    _backDecoration = null;

    TodoOperation.db.getPanelList().then((dataModelList) {
      TodoOperation.db.getPositions().then((positions) {
        if (positions.length == dataModelList.length) {
          positions.forEach(
            (position) {
              todoPanelWidgets.add(
                _createPanelWidget(
                  dataModelList.firstWhere(
                      (element) => element.panelId == position.position),
                ),
              );
            },
          );
        } else {
          todoPanelWidgets =
              dataModelList.map((e) => _createPanelWidget(e)).toList();
          _savePositions();
        }
        setState(() {});
      });

      if (dataModelList.length == 0) {
        _backDecoration = DecorationImage(
          image: AssetImage("assets/icons/icon_emptylist.png"),
          fit: BoxFit.scaleDown,
        );
      } else {
        _backDecoration = null;
      }
    });
  }

  _hideSelectedItem() {
    _isTodoItemSelected = false;
    _selectedCustomPanelItem = null;
    _selectedCustomPanelController?.unHighlightPanel();
    _selectedCustomPanelController = null;
    _animOpacity = 0.0;
    _floatMarginBottom = 0.0;
    setState(() {});
  }

  _savePositions() {
    TodoOperation.db.clearPositions().then((value) {
      todoPanelWidgets.forEach((element) {
        TodoOperation.db
            .addPosition(TodoPanelPositionDataModel(element.panelId));
      });
    });
  }

  _addPanel(TodoPanelDataModel dataModel) {
    todoPanelWidgets.add(_createPanelWidget(dataModel));
    if (todoPanelWidgets.length != 0) {
      _backDecoration = null;
    }
    TodoOperation.db.addPosition(TodoPanelPositionDataModel(dataModel.panelId));
    setState(() {});
  }

  _deletePanel(CustomPanel panel) {
    TodoOperation.db.removePanel(panel.panelId);
    todoPanelWidgets.remove(panel);
    if (todoPanelWidgets.length == 0) {
      _backDecoration = DecorationImage(
        image: AssetImage("assets/icons/icon_emptylist.png"),
        fit: BoxFit.scaleDown,
      );
    }
    _savePositions();
    setState(() {});
  }

  CustomPanel _createPanelWidget(TodoPanelDataModel todoPanelDataModel) {
    return CustomPanel(
      deletePanel: _deletePanel,
      panelId: todoPanelDataModel.panelId,
      key: ObjectKey(todoPanelDataModel),
      listName: todoPanelDataModel.name,
      onSelectedItem: _onSelectedItem,
      onTap: _onTapPanel,
      colorPanel: Color(todoPanelDataModel.colorBack),
    );
  }

  _onSelectedItem(CustomPanelItem customPanelItem,
      CustomPanelController customPanelController) {
    if (!_isTodoItemSelected) {
      customPanelController.highlightPanel();
      _selectedCustomPanelItem = customPanelItem;
      _selectedCustomPanelController = customPanelController;
      customPanelController.removeItem(customPanelItem);
      _isTodoItemSelected = true;
      _animOpacity = 1.0;
      _floatMarginBottom = 50.0;
      setState(() {});
    }
  }

  _onTapPanel(CustomPanelController customPanelController) {
    if (_isTodoItemSelected) {
      customPanelController.addItem(_selectedCustomPanelItem.todoItem);
      _selectedCustomPanelController.unHighlightPanel();
      _selectedCustomPanelController = null;
      _isTodoItemSelected = false;
      _animOpacity = 0.0;
      _floatMarginBottom = 0.0;
      Future.delayed(Duration(milliseconds: _animOpacityMilis + 50), () {
        setState(() {
          _selectedCustomPanelItem = null;
        });
      });
      setState(() {});
    }
  }

  _addNewPanel(String name, Color color) {
    TodoOperation.db
        .addPanel(TodoPanelDataModel(name, color.value))
        .then((dataModel) {
      _addPanel(dataModel);
    });
  }

  _onReorder(int oldIndex, int newIndex) {
    if (newIndex > oldIndex) {
      // bu kısım olmaz ise bazı durumlarda iki item üst üste biniyor
      newIndex -= 1;
    }

    Widget tempWidget = todoPanelWidgets.removeAt(oldIndex);
    todoPanelWidgets.insert(newIndex, tempWidget);
    _savePositions();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.name),
      ),
      floatingActionButton: Container(
        margin: EdgeInsets.only(bottom: _floatMarginBottom),
        child: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  content: Stack(
                    children: <Widget>[
                      Positioned(
                        right: -40.0,
                        top: -40.0,
                        child: InkResponse(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: CircleAvatar(
                            child: Icon(Icons.add),
                            backgroundColor: Colors.red,
                          ),
                        ),
                      ),
                      Form(
                        child: Scrollbar(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                height: 130,
                                child: BlockPicker(
                                  pickerColor: pickerColor,
                                  onColorChanged: (a) {
                                    pickerColor = a;
                                  },
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.all(8.0),
                                child: TextFormField(
                                  onFieldSubmitted: (text) {
                                    if (text.isNotEmpty) {
                                      _addNewPanel(text, pickerColor);
                                      _textEditingController.text = "";
                                      Navigator.of(context).pop();
                                    }
                                  },
                                  textInputAction: TextInputAction.go,
                                  controller: _textEditingController,
                                  cursorColor: Colors.black,
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.only(
                                          left: 15,
                                          bottom: 11,
                                          top: 11,
                                          right: 15),
                                      hintText: "Panel isim"),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: RaisedButton(
                                  child: Text("Oluştur"),
                                  onPressed: () {
                                    if (_textEditingController
                                        .text.isNotEmpty) {
                                      _addNewPanel(_textEditingController.text,
                                          pickerColor);
                                      _textEditingController.text = "";
                                      Navigator.of(context).pop();
                                    }
                                  },
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          },
        ),
      ),
      body: Container(
        color: Color(0xffdce3e9),
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                image: _backDecoration,
              ),
              height: double.infinity,
              width: double.infinity,
              child: Theme(
                data: ThemeData(canvasColor: Colors.transparent),
                child: ReorderableListView(
                  onReorder: _onReorder,
                  children: todoPanelWidgets,
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Center(
                  child: AnimatedOpacity(
                    duration: Duration(milliseconds: _animOpacityMilis),
                    curve: Curves.easeIn,
                    opacity: _animOpacity,
                    child: Container(
                      margin: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Expanded(
                            flex: 5,
                            child: Container(child: _selectedCustomPanelItem),
                          ),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.all(3),
                              child: IconButton(
                                color: Colors.red,
                                icon: Icon(Icons.delete),
                                tooltip: 'Sil',
                                onPressed: () {
                                  _hideSelectedItem();
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
