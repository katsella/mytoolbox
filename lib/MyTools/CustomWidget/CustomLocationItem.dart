import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:mytoolbox/Database/LocationModel/LocationItemModel.dart';

class CustomLocationItem extends StatefulWidget {
  CustomLocationItem(
      {@required Key key, @required this.modelItem, @required this.deleteItem})
      : super(key: key);
  final LocationItemDataModel modelItem;
  final Function(CustomLocationItem) deleteItem;
  _StateCustomLocationItem createState() => _StateCustomLocationItem();
}

class _StateCustomLocationItem extends State<CustomLocationItem> {
  Widget build(BuildContext context) {
    return Dismissible(
      key: ObjectKey(widget.modelItem),
      onDismissed: (direction) {
        widget.deleteItem(widget);
      },
      background: Container(color: Colors.transparent),
      child: Container(
        margin: EdgeInsets.all(6),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.4),
              spreadRadius: 3,
              blurRadius: 4,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ],
        ),
        child: GestureDetector(
          onTap: () {
            MapsLauncher.launchCoordinates(widget.modelItem.latitude,
                widget.modelItem.longitude, widget.modelItem.name);
          },
          child: ListTile(
            title: Text(
              widget.modelItem.name,
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
      ),
    );
  }
}
