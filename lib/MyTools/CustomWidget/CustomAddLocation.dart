import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:mytoolbox/Database/LocationModel/LocationItemModel.dart';

class CustomAddLocation extends StatefulWidget {
  CustomAddLocation({Key key, this.locationAdd}) : super(key: key);
  final Function(LocationItemDataModel) locationAdd;
  _StateCustomAddLocation createState() => _StateCustomAddLocation();
}

class _StateCustomAddLocation extends State<CustomAddLocation> {
  Position currentLocation;
  String _textLatitude = "Bekleyiniz", _textLongitude = "Bekleyiniz";
  bool _isLocationSetted = false,
      _isTextFilled = false,
      _isButtonEnable = false;
  double _buttonOpacity = 0.5;
  TextEditingController _textEditingController = TextEditingController();

  _StateCustomAddLocation() {
    _getPosition().then((value) {
      currentLocation = value;
      _textLatitude = value.latitude.toString();
      _textLongitude = value.longitude.toString();
      _isLocationSetted = true;
      _checkButtonEnable();
      setState(() {});
    });
  }

  @override
  void setState(function) {
    if (mounted) {
      super.setState(function);
    }
  }

  _setIsTextFilled(bool state) {
    if (_isTextFilled == state) return;
    _isTextFilled = state;
    _checkButtonEnable();
    setState(() {});
  }

  _checkButtonEnable() {
    if (_isLocationSetted && _isTextFilled) {
      _isButtonEnable = true;
      _buttonOpacity = 1.0;
    } else {
      _isButtonEnable = false;
      _buttonOpacity = 0.5;
    }
  }

  Future<Position> _getPosition() async {
    return await Geolocator.getCurrentPosition();
  }

  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Form(
          child: Scrollbar(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Enlem: " + _textLatitude),
                Text("Boylam: " + _textLongitude),
                TextFormField(
                  controller: _textEditingController,
                  onChanged: (text) {
                    _setIsTextFilled(text.isNotEmpty);
                  },
                  maxLength: 20,
                  decoration: InputDecoration(
                    labelText: 'Konum İsmi',
                    labelStyle: TextStyle(
                      color: Color(0xFF6200EE),
                    ),
                    helperText: 'Karekter',
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFF6200EE)),
                    ),
                  ),
                ),
                Row(
                  children: [
                    RaisedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      color: Colors.redAccent,
                      textColor: Colors.white,
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                      child: Text('İptal'),
                    ),
                    Spacer(),
                    Opacity(
                      opacity: _buttonOpacity,
                      child: RaisedButton(
                        onPressed: () {
                          if (!_isButtonEnable) return;
                          widget.locationAdd(LocationItemDataModel(
                              _textEditingController.text,
                              currentLocation.latitude,
                              currentLocation.longitude));
                          Navigator.of(context).pop();
                        },
                        color: Colors.redAccent,
                        textColor: Colors.white,
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        child: Text('Ekle'),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
