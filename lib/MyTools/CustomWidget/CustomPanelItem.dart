import 'package:flutter/material.dart';

class CustomPanelItem extends StatefulWidget {
  CustomPanelItem({Key key, @required this.itemId, this.todoItem, this.longPress}) : super(key: key);
  final int itemId;
  final TodoItem todoItem;
  final Function(CustomPanelItem) longPress;
  _CustomPanelteState createState() => _CustomPanelteState(todoItem);
}

class TodoItem {
  int todoItemId;
  String text;
  TodoItem(this.todoItemId, this.text);
}

class _CustomPanelteState extends State<CustomPanelItem> {
  TodoItem todoItem;
  Widget todoWidget;
  Color _colorBack = Colors.white;
  double width;
  _CustomPanelteState(this.todoItem);

  @override
  void setState(function) {
    if (mounted) {
      super.setState(function);
    }
  }

  _onTapDown(a) {
    _colorBack = Colors.grey;
    setState(() {});
  }

  _onTapUp(a) {
    _colorBack = Colors.white;
    setState(() {});
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        widget?.longPress(widget);
      },
      onTapDown: _onTapDown,
      onTapCancel: () => _onTapUp(0),
      onTapUp: _onTapUp,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 600),
        curve: Curves.fastOutSlowIn,
        decoration: BoxDecoration(
          color: _colorBack,
          border: Border.all(
            color: Colors.black,
          ),
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        child: Center(
          child: Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8, bottom: 8),
            padding: EdgeInsets.only(left: 10),
            child: Text(
              todoItem.text,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
