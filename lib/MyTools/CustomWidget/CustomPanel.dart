import 'package:flutter/material.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:mytoolbox/Database/todoDataModel/TodoItemDataModel.dart';
import 'package:mytoolbox/Database/todoDataModel/TodoOperation.dart';
import 'package:mytoolbox/MyTools/CustomWidget/CustomPanelItem.dart';

abstract class CustomPanelController {
  addItem(TodoItem item);
  removeItem(CustomPanelItem customPanelItem);
  highlightPanel();
  unHighlightPanel();
}

class CustomPanel extends StatefulWidget {
  CustomPanel(
      {Key key,
      @required this.panelId,
      this.listName,
      this.widthMax,
      this.colorPanel = const Color(0xffebecf0),
      this.onSelectedItem,
      this.onTap,
      @required this.deletePanel})
      : super(key: key);
  final int panelId;
  final String listName;
  final int widthMax;
  final Color colorPanel;
  final Function(CustomPanelItem, CustomPanelController) onSelectedItem;
  final Function(CustomPanelController) onTap;
  final Function(CustomPanel) deletePanel;
  _CustomPanelState createState() => _CustomPanelState(colorPanel, panelId);
}

class _CustomPanelState extends State<CustomPanel> with CustomPanelController {
  List<CustomPanelItem> todoWidgetList;
  Color _colorPanel;
  Widget _addItemWidget;
  Widget _textBoxWidget;
  TextEditingController _textEditingController = TextEditingController();
  Widget _addWidgetPlacer;

  _CustomPanelState(this._colorPanel, int panelId) {
    todoWidgetList = [];

    _addItemWidget = GestureDetector(
      onTap: _showAddBox,
      child: Container(
        height: 40,
        child: Center(
          child: Container(
            padding: EdgeInsets.only(left: 15),
            width: double.infinity,
            child: Row(
              children: [
                Icon(Icons.add_outlined),
                Text(
                  "Yeni kart ekle",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w700),
                ),
              ],
            ),
          ),
        ),
      ),
    );
    _addWidgetPlacer = _addItemWidget;

    _textBoxWidget = Column(
      children: [
        Container(
          width: double.infinity,
          margin: EdgeInsets.only(top: 7, bottom: 2),
          padding: EdgeInsets.only(left: 5, right: 5),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(5)),
            border: Border.all(
              color: Colors.black,
            ),
          ),
          child: TextField(
            onSubmitted: (text) {
              if (text.isNotEmpty) {
                addItem(TodoItem(-1, text));
                _textEditingController.text = "";
              }
            },
            autofocus: true,
            onEditingComplete: () {}, // do nothing
            textInputAction: TextInputAction
                .send, // Klavyeden submit tusuna basinca klavye kapanmiyor
            minLines: 3,
            maxLines: 5,
            controller: _textEditingController,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Kart',
              isDense: true,
            ),
            style: TextStyle(
              fontSize: 18,
              color: Colors.black,
            ),
          ),
        ),
        Container(
          width: double.infinity,
          margin: EdgeInsets.only(bottom: 2, left: 10, right: 10),
          child: SizedBox(
            width: 100,
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: RaisedButton(
                    color: Color(0xff5aac44),
                    onPressed: () {
                      if (_textEditingController.text.isNotEmpty) {
                        addItem(TodoItem(-1, _textEditingController.text));
                        _textEditingController.text = "";
                      }
                    },
                    onLongPress: () {},
                    child: Text(
                      "Ekle",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );

    TodoOperation.db.getTodoList(panelId).then((todoItems) {
      todoWidgetList =
          todoItems.map((e) => _getItem(TodoItem(e.itemId, e.text))).toList();
      setState(() {});
    });
  }
  int keyboardListenerId;
  @override
  void initState() {
    super.initState();
    keyboardListenerId = KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        if (!visible) {
          setState(() {
            _addWidgetPlacer = _addItemWidget;
          });
        }
      },
    );
  }

  @override
  void dispose() {
    KeyboardVisibilityNotification().removeListener(keyboardListenerId);
    super.dispose();
  }

  @override
  void setState(function) {
    if (mounted) {
      super.setState(function);
    }
  }

  _showAddBox() {
    _addWidgetPlacer = _textBoxWidget;
    setState(() {});
  }

  @override
  addItem(TodoItem item) {
    // _addWidgetPlacer = _addItemWidget;
    TodoOperation.db
        .addTodoItem(TodoItemDataModel(widget.panelId, item.text))
        .then((adedItem) {
      item.todoItemId = adedItem.itemId;
      todoWidgetList.add(CustomPanelItem(
        itemId: adedItem.itemId,
        key: ObjectKey(item),
        todoItem: item,
        longPress: _itemLongPress,
      ));
      setState(() {});
    });
  }

  @override
  removeItem(CustomPanelItem customPanelItem) {
    TodoOperation.db
        .removeTodoItem(
      customPanelItem.todoItem.todoItemId,
    )
        .then((isDeleted) {
      todoWidgetList.remove(customPanelItem);
      setState(() {});
    });
  }

  @override
  highlightPanel() {
    _colorPanel = _colorPanel.withOpacity(0.3);
    setState(() {});
  }

  @override
  unHighlightPanel() {
    _colorPanel = _colorPanel.withOpacity(1.0);
    setState(() {});
  }

  _deleteThisPanel() {
    widget.deletePanel(widget);
  }

  CustomPanelItem _getItem(TodoItem item) {
    // sadece constuructor da eklemek için
    return CustomPanelItem(
      itemId: item.todoItemId,
      key: ObjectKey(UniqueKey()),
      todoItem: item,
      longPress: _itemLongPress,
    );
  }

  _itemLongPress(CustomPanelItem customPanelItem) {
    widget?.onSelectedItem(
        customPanelItem, this); //CustomPanelItem ve customPanelController
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _addWidgetPlacer = _addItemWidget;
        });
        widget.onTap(this);
      },
      child: Container(
        constraints: BoxConstraints(
          maxHeight: double.infinity,
        ),
        decoration: BoxDecoration(
          color: _colorPanel,
          borderRadius: BorderRadius.all(Radius.circular(5)),
          border: Border.all(
            color: Colors.black,
          ),
        ),
        margin: EdgeInsets.only(top: 10, left: 10, right: 10),
        padding: EdgeInsets.only(
          left: 10,
          right: 10,
        ),
        child: Wrap(
          children: [
            Row(
              children: [
                Expanded(
                  child: Container(
                    height: 50,
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.only(left: 15),
                        width: double.infinity,
                        child: Text(
                          widget.listName,
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: _deleteThisPanel,
                  child: Icon(Icons.delete_forever_outlined),
                ),
              ],
            ),
            Wrap(
              runSpacing: 7,
              children: todoWidgetList,
            ),
            Wrap(
              children: [_addWidgetPlacer],
            ),
          ],
        ),
      ),
    );
  }
}
