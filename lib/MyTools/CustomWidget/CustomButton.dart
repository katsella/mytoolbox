import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  CustomButton(
      {Key key,
      this.text,
      this.backColor,
      this.backColorAnim,
      this.textColor,
      this.onTap,
      this.onLongPressRepeat,
      this.width,
      this.heigth,
      this.margin,
      this.padding})
      : super(key: key);
  final String text;
  final Color backColor, backColorAnim, textColor;
  final Function onTap, onLongPressRepeat;
  final double width, heigth, margin, padding;
  _StateCustomButton createState() =>
      _StateCustomButton(this.backColor, this.padding);
}

class _StateCustomButton extends State<CustomButton> {
  Color _backColor = Color(0xfff3f2f3);
  double _textPadding = 10;
  int animMilis = 100;
  bool isAnimationRunning = false, buttonDown = false;

  _StateCustomButton(this._backColor, this._textPadding);
  _onTapDown(detail) {
    setState(() {
      _backColor = widget.backColorAnim;
      _textPadding *= 1.13;
      isAnimationRunning = true;
      buttonDown = true;
      Future.delayed(Duration(milliseconds: animMilis), () {
        isAnimationRunning = false;
        if (!buttonDown) _animationStop();
      });
    });
  }

  _startLongPressRepeat() {
    // silme tusuna basili tutuldugunda
    if (buttonDown)
      Future.delayed(Duration(milliseconds: 200), () {
        if (!buttonDown) return;
        widget?.onLongPressRepeat();
        _startLongPressRepeat();
      });
  }

  _onTapUp(detail) {
    buttonDown = false;
    _animationStop();
  }

  _onTapCancel() {
    buttonDown = false;
    _animationStop();
  }

  _animationStop() {
    if (!isAnimationRunning)
      setState(() {
        _backColor = widget.backColor;
        _textPadding /= 1.13;
      });
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      onTapDown: _onTapDown,
      onTapUp: _onTapUp,
      onTapCancel: _onTapCancel,
      onLongPressMoveUpdate: _startLongPressRepeat(),
      child: AnimatedContainer(
        width: widget.width,
        height: widget.heigth,
        margin: EdgeInsets.all(widget.margin),
        duration: Duration(milliseconds: animMilis),
        curve: Curves.easeInExpo,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(widget.width / 2.5),
          color: _backColor,
        ),
        child: AnimatedContainer(
          duration: Duration(milliseconds: animMilis),
          curve: Curves.easeInExpo,
          padding: EdgeInsets.all(_textPadding),
          child: FittedBox(
            child: Center(
              child: Text(
                widget.text,
                style: TextStyle(color: widget.textColor),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
