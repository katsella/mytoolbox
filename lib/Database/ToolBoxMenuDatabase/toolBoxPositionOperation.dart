import 'package:mytoolbox/Database/ToolBoxMenuDatabase/ToolBoxPositionDataModel.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class ToolBoxPositionOperation {
  final String dbName = "AlpMainDb.db";
  Database _database;
  static const String _tableToolPosition = "ToolPosition";
  static const String _columnIndex = "[index]";
  static const String _columnPosition = "[position]";

  ToolBoxPositionOperation._();
  static final ToolBoxPositionOperation db = ToolBoxPositionOperation._();

  Future<Database> get database async {
    if (_database == null) _database = await createDatabase();
    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();
    return await openDatabase(join(dbPath, dbName), version: 1,
        onCreate: (db, version) async {
      await db.execute(
        '''CREATE TABLE $_tableToolPosition ( 
          $_columnIndex int NOT NULL PRIMARY KEY, 
          $_columnPosition int NOT NULL);''',
      );
    });
  }

  Future<List<ToolboxPositionDataModel>> getPositionList() async {
    final db = await database;
    List<Map> dataMap =
        await db.query(_tableToolPosition, orderBy: "$_columnIndex");
    return dataMap
        .map((e) => ToolboxPositionDataModel.fromJson(e))
        .toList();
  }

  Future<bool> addOrUpdatePosition(
      ToolboxPositionDataModel toolboxPositionDataModel) async {
    final db = await database;
    int dataMap;
    var getData = await db.query(_tableToolPosition,
        where: "$_columnIndex=?", whereArgs: [toolboxPositionDataModel.index]);
    if (getData.isEmpty) {
      dataMap = await db.insert(
          _tableToolPosition, toolboxPositionDataModel.toJson());
    } else {
      dataMap = await db.update(
          _tableToolPosition, toolboxPositionDataModel.toJson(),
          where: "$_columnIndex = ?",
          whereArgs: [toolboxPositionDataModel.index]);
    }
    return dataMap != null;
  }

  Future<bool> addIfNotExistPosition(
      ToolboxPositionDataModel toolboxPositionDataModel) async {
    final db = await database;
    int dataMap;
    var getData = await db.query(_tableToolPosition,
        where: "$_columnIndex=?", whereArgs: [toolboxPositionDataModel.index]);
    if (getData.isEmpty) {
      dataMap = await db.insert(
          _tableToolPosition, toolboxPositionDataModel.toJson());
    } else {
      return false;
    }
    return dataMap != null;
  }

  Future<bool> trimTable(int listLength) async {
    final db = await database;
    var getData = await db.delete(_tableToolPosition, where: "$_columnPosition > ?", whereArgs: [listLength]);
    return getData != null;
  }

  Future<bool> clearTable() async {
    final db = await database;
    var getData = await db.delete(_tableToolPosition);
    return getData != null;
  }

  Future<void> close() async {
    await _database.close();
  }
}
