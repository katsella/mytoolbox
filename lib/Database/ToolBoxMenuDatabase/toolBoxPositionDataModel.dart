class ToolboxPositionDataModel {
  int index, position;

  ToolboxPositionDataModel(this.index, this.position);

  ToolboxPositionDataModel.fromJson(Map<String, dynamic> json) {
    index = json['index'];
    position = json['position'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['index'] = this.index;
    data['position'] = this.position;
    return data;
  }
}
