class TodoItemDataModel {
  int itemId;
  int panelId;
  String text;

  TodoItemDataModel(this.panelId, this.text);

  TodoItemDataModel.fromJson(Map<String, dynamic> json) {
    itemId = json['itemId'];
    panelId = json['panelId'];
    text = json['text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['panelId'] = this.panelId;
    data['text'] = this.text;
    return data;
  }
}
