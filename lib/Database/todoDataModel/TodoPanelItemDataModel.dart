class TodoPanelDataModel {
  int panelId;
  String name;
  int colorBack;

  TodoPanelDataModel(this.name, this.colorBack);

  TodoPanelDataModel.fromJson(Map<String, dynamic> json) {
    panelId = json['panelId'];
    name = json['name'];
    colorBack = json['colorBack'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['panelId'] = this.panelId;
    data['name'] = this.name;
    data['colorBack'] = this.colorBack;
    return data;
  }
}
