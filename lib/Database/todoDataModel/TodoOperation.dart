import 'package:mytoolbox/Database/todoDataModel/TodoIPanelPositionDataModel.dart';
import 'package:mytoolbox/Database/todoDataModel/TodoItemDataModel.dart';
import 'package:mytoolbox/Database/todoDataModel/TodoPanelItemDataModel.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class TodoOperation {
  final String dbName = "AlpTodoDb.db";
  Database _database;
  static const String _tablePanelList = "[PanelList]";
  static const String _columnPanelId = "[panelId]";
  static const String _columnName = "[name]";
  static const String _columnColorBack = "[colorBack]";

  static const String _tablePanelItems = "[PanelItems]";
  static const String _columnPItemId = "[itemId]";
  static const String _columnPItemText = "[text]";

  static const String _tablePositions = "[PanelPosition]";
  static const String _columnPositionId = "[id]";
  static const String _columnPosition = "[position]";

  TodoOperation._();
  static final TodoOperation db = TodoOperation._();

  Future<Database> get database async {
    if (_database == null) _database = await createDatabase();
    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();
    return await openDatabase(
      join(dbPath, dbName),
      version: 1,
      onCreate: (db, version) async {
        await db.execute(
          '''CREATE TABLE $_tablePanelList ( 
          $_columnPanelId INTEGER PRIMARY KEY AUTOINCREMENT,
          $_columnName varchar(500) NOT NULL, 
          $_columnColorBack int NOT NULL);''',
        );
        await db.execute(
          '''CREATE TABLE $_tablePanelItems ( 
          $_columnPItemId INTEGER PRIMARY KEY AUTOINCREMENT,
          $_columnPanelId INTEGER NOT NULL,
          $_columnPItemText varchar(1000) NOT NULL, 
          FOREIGN KEY($_columnPanelId) REFERENCES $_tablePanelList($_columnPanelId));''',
        );
        await db.execute(
          '''CREATE TABLE $_tablePositions ( 
          $_columnPositionId INTEGER PRIMARY KEY AUTOINCREMENT,
          $_columnPosition INTEGER NOT NULL UNIQUE,
          FOREIGN KEY($_columnPosition) REFERENCES $_tablePanelList($_columnPanelId));''',
        );
      },
    );
  }

  Future<List<TodoPanelDataModel>> getPanelList() async {
    final db = await database;
    List<Map> dataMap =
        await db.query(_tablePanelList, orderBy: "$_columnPanelId");
    return dataMap.map((e) => TodoPanelDataModel.fromJson(e)).toList();
  }

  Future<TodoPanelDataModel> addPanel(
      TodoPanelDataModel todoPanelItemDataModel) async {
    final db = await database;
    int id = await db.insert(_tablePanelList, todoPanelItemDataModel.toJson());
    todoPanelItemDataModel.panelId = id;
    return todoPanelItemDataModel;
  }

  Future<bool> removePanel(int panelId) async {
    final db = await database;

    await db.delete(_tablePanelItems,
        where: "$_columnPanelId = ?",
        whereArgs: [panelId]); // panele ait bütün kayıtlar siliniyor.

    int id = await db.delete(_tablePanelList,
        where: "$_columnPanelId = ?", whereArgs: [panelId]);
    return id != null;
  }

//-----------------------Item List--------------------------

  Future<List<TodoItemDataModel>> getTodoList(int panelId) async {
    final db = await database;
    List<Map> dataMap = await db.query(_tablePanelItems,
        where: "$_columnPanelId=?",
        whereArgs: [panelId],
        orderBy: "$_columnPItemId");
    return dataMap.map((e) => TodoItemDataModel.fromJson(e)).toList();
  }

  Future<TodoItemDataModel> addTodoItem(
      TodoItemDataModel todoPanelItemDataModel) async {
    final db = await database;
    int id = await db.insert(_tablePanelItems, todoPanelItemDataModel.toJson());
    todoPanelItemDataModel.itemId = id;
    return todoPanelItemDataModel;
  }

  Future<bool> removeTodoItem(int itemId) async {
    final db = await database;
    int id = await db.delete(_tablePanelItems,
        where: "$_columnPItemId = ?", whereArgs: [itemId]);
    return id != null;
  }

//----------------------Panel Position---------------------

  Future<List<TodoPanelPositionDataModel>> getPositions() async {
    final db = await database;
    List<Map> dataMap =
        await db.query(_tablePositions, orderBy: "$_columnPositionId");
    return dataMap.map((e) => TodoPanelPositionDataModel.fromJson(e)).toList();
  }

  Future<TodoPanelPositionDataModel> addPosition(
      TodoPanelPositionDataModel todoPanelPositionDataModel) async {
    final db = await database;
    int id =
        await db.insert(_tablePositions, todoPanelPositionDataModel.toJson());
    todoPanelPositionDataModel.id = id;
    return todoPanelPositionDataModel;
  }

  Future<bool> removePosition(int positionId) async {
    final db = await database;
    int id = await db.delete(_tablePositions,
        where: "$_columnPositionId = ?", whereArgs: [positionId]);
    return id != null;
  }

  Future<bool> clearPositions() async {
    final db = await database;
    int id = await db.delete(
      _tablePositions,
    );
    return id != null;
  }

//-------------------------General---------------------------
  Future<bool> clearTable() async {
    final db = await database;
    var getData = await db.delete(_tablePanelList);
    return getData != null;
  }

  Future<void> close() async {
    await _database.close();
  }
}
