class TodoPanelPositionDataModel {
  int id, position;

  TodoPanelPositionDataModel(this.position);

  TodoPanelPositionDataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    position = json['position'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['position'] = this.position;
    return data;
  }
}
