class LocationListPositionDataModel {
  int positionId, locationId;

  LocationListPositionDataModel(this.locationId);

  LocationListPositionDataModel.fromJson(Map<String, dynamic> json) {
    positionId = json['positionId'];
    locationId = json['locationId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['positionId'] = this.positionId;
    data['locationId'] = this.locationId;
    return data;
  }
}
