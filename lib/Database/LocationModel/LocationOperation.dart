import 'package:mytoolbox/Database/LocationModel/LocationItemModel.dart';
import 'package:mytoolbox/Database/LocationModel/LocationListPositionDataModel.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class LocationOperation {
  final String dbName = "AlpLocationDb.db";
  Database _database;
  static const String _tableLocationList = "[LocationList]";
  static const String _columnLocationId = "[locationId]";
  static const String _columnName = "[name]";
  static const String _columnLatitude = "[latitude]";
  static const String _columnLongitude = "[longitude]";

  static const String _tableListPosition = "[LocationPositionList]";
  static const String _columnListPositionId = "[positionId]";

  LocationOperation._();
  static final LocationOperation db = LocationOperation._();

  Future<Database> get database async {
    if (_database == null) _database = await createDatabase();
    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();
    return await openDatabase(
      join(dbPath, dbName),
      version: 1,
      onCreate: (db, version) async {
        await db.execute(
          '''CREATE TABLE $_tableLocationList ( 
          $_columnLocationId INTEGER PRIMARY KEY AUTOINCREMENT,
          $_columnName varchar(500) NOT NULL, 
          $_columnLatitude double(4,8) NOT NULL,
          $_columnLongitude double(4,8) NOT NULL);''',
        );

        await db.execute(
          '''CREATE TABLE $_tableListPosition ( 
          $_columnListPositionId INTEGER PRIMARY KEY AUTOINCREMENT,
          $_columnLocationId INTEGER NOT NULL UNIQUE,
          FOREIGN KEY($_columnLocationId) REFERENCES $_tableLocationList($_columnLocationId));''',
        );
      },
    );
  }

  Future<List<LocationItemDataModel>> getLocationList() async {
    final db = await database;
    List<Map> dataMap =
        await db.query(_tableLocationList, orderBy: "$_columnLocationId");
    return dataMap.map((e) => LocationItemDataModel.fromJson(e)).toList();
  }

  Future<LocationItemDataModel> addLocation(
      LocationItemDataModel locationItemDataModel) async {
    final db = await database;
    int id =
        await db.insert(_tableLocationList, locationItemDataModel.toJson());
    locationItemDataModel.locationId = id;
    return locationItemDataModel;
  }

  Future<bool> removeLocation(int locationId) async {
    final db = await database;

    await db.delete(_tableListPosition,
        where: "$_columnLocationId = ?",
        whereArgs: [
          locationId
        ]); // panele ait, position listdeki bütün kayıtlar siliniyor.

    int id = await db.delete(_tableLocationList,
        where: "$_columnLocationId = ?", whereArgs: [locationId]);
    return id != null;
  }

  //----------------------Locataion List Position---------------------

  Future<List<LocationListPositionDataModel>> getPositions() async {
    final db = await database;
    List<Map> dataMap =
        await db.query(_tableListPosition, orderBy: "$_columnListPositionId");
    return dataMap
        .map((e) => LocationListPositionDataModel.fromJson(e))
        .toList();
  }

  Future<LocationListPositionDataModel> addPosition(
      LocationListPositionDataModel todoPanelPositionDataModel) async {
    final db = await database;
    int id = await db.insert(
        _tableListPosition, todoPanelPositionDataModel.toJson());
    todoPanelPositionDataModel.positionId = id;
    return todoPanelPositionDataModel;
  }

  Future<bool> removePosition(int positionId) async {
    final db = await database;
    int id = await db.delete(_tableListPosition,
        where: "$_columnListPositionId = ?", whereArgs: [positionId]);
    return id != null;
  }

  Future<bool> clearPositions() async {
    final db = await database;
    int id = await db.delete(
      _tableListPosition,
    );
    return id != null;
  }

//-------------------------General---------------------------
  Future<bool> clearTable() async {
    final db = await database;
    var getData = await db.delete(_tableLocationList);
    return getData != null;
  }

  Future<void> close() async {
    await _database.close();
  }
}
