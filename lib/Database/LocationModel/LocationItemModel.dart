class LocationItemDataModel {
  int locationId;
  String name;
  double latitude, longitude;

  LocationItemDataModel(this.name, this.latitude, this.longitude);

  LocationItemDataModel.fromJson(Map<String, dynamic> json) {
    locationId = json['locationId'];
    name = json['name'];
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['locationId'] = this.locationId;
    data['name'] = this.name;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}
